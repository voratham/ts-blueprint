import { masterDataDomain } from './masterData';

describe('MasterData domain', () => {
  it('method create. should create new items from revisionInput and items', () => {
    const revisionInput = {
      companyId: '111',
      projectId: '222',
      collectionName: 'task-type',
      versions: [1, 2, 3],
    };

    const items = [
      {
        name: 'AA',
        description: 'This is AA',
      },
      {
        name: 'BB',
        description: 'This is BB',
      },
    ];

    expect(masterDataDomain.create(revisionInput, items)).toEqual([
      {
        collectionName: 'task-type',
        companyId: '111',
        item: { description: 'This is AA', name: 'AA' },
        projectId: '222',
        revision: 3,
      },
      {
        collectionName: 'task-type',
        companyId: '111',
        item: { description: 'This is BB', name: 'BB' },
        projectId: '222',
        revision: 3,
      },
    ]);
  });
});
