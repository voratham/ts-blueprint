export interface ValidatePass {
  isValidateFailed: boolean;
}

export interface ValidateFailed {
  isValidateFailed: boolean;
  error: {
    [key: string]: string;
  };
}

export interface RequireFieldProps {
  key: string;
  value: any;
  optional?: boolean;
}

export type DataType =
  | 'ObjectId'
  | 'Number'
  | 'String'
  | 'Boolean'
  | 'Array'
  | 'Enum'
  | 'Object';

export interface ValidateObject {
  [key: string]: {
    type?: string;
    optional?: boolean;
    enum?: Array<string>;
  };
}

export interface HasKeyValue {
  key: string;
  value: any;
  validate?: ValidateObject;
}

export interface ValidateInputParams {
  key: string;
  objectInput: { [key: string]: any };
  validateObject: ValidateObject;
}
