import { Env, loadConfig } from '@tel/env-decorator';
import * as dotenv from 'dotenv';

dotenv.config();

export class AppConfig {
  @Env(['npm_package_name'], { required: true })
  APP_NAME: string;

  @Env()
  SERVER_ENABLED: boolean = true;

  @Env()
  SERVER_PORT: number = 8080;

  @Env()
  SERVER_HOST: string = '0.0.0.0';

  @Env(['MONGO_URI', 'DB_URI'], { required: true })
  MONGO_URI: string;

  @Env()
  SERVICE_4PL_IMPORT_URI: string;
}

export const config = loadConfig(AppConfig);
