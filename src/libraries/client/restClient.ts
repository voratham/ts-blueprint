import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  Method,
} from 'axios';

export interface ILogger {
  error(obj: Object, ...params: any[]): void;
  info(obj: Object, ...params: any[]): void;
}

export interface IConfig {
  baseURL?: string;
  headers?: any;
  notFoundAsNull?: boolean;
}

export interface IRequest {
  url?: string;
  method?: Method;
  headers?: any;
  params?: Params;
  data?: any;
}

export interface IRestClient {
  request<ResData>(request: IRequest): Promise<ResData>;
  get<ResData>(url: string, request: IRequest): Promise<ResData>;
  post<ResData>(url: string, request: IRequest): Promise<ResData>;
  put<ResData>(url: string, request: IRequest): Promise<ResData>;
  delete<ResData>(url: string, request: IRequest): Promise<ResData>;
}

export type Params = { [key: string]: string | number | boolean };

export class ResponseError extends Error {
  status: number;
  statusText?: string;
  data?: any;
  headers?: any;
  request: AxiosRequestConfig;

  constructor(
    request: AxiosRequestConfig,
    status: number,
    statusText?: string,
    headers?: any,
    data?: any,
  ) {
    super(`${status} ${statusText}`);
    this.status = status;
    this.statusText = statusText;
    this.headers = headers;
    this.data = data;
    this.request = request;
  }
}

export class RequestError extends Error {
  message: string;
  request: AxiosRequestConfig;

  constructor(request: AxiosRequestConfig, message: string) {
    super(message);
    this.message = message;
    this.request = request;
  }
}

function handleError(error: any, config: IConfig = {}, logger: ILogger) {
  logger.error('HTTP_RESPONSE_ERROR:: ', JSON.stringify(error));
  if (error.response) {
    if (error.response.status === 404 && config.notFoundAsNull === true) {
      return null;
    }

    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    const { status, statusText, headers, data } = error.response;
    return Promise.reject(
      new ResponseError(error.config, status, statusText, headers, data),
    );
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    const { message } = error;
    return Promise.reject(new RequestError(error.config, message));
  }

  return Promise.reject(error);
}

function handleResponse<ResData>(
  response: AxiosResponse<ResData>,
  logger: ILogger,
) {
  logger.info('HTTP_REQUEST:: ', JSON.stringify(response));
  return response.data;
}

export class NoopLogger implements ILogger {
  info(_obj: any, ..._params: any[]) {}
  error(_obj: Object, ..._params: any[]) {}
}

export class RestClient implements IRestClient {
  private axios: AxiosInstance;
  private logger: ILogger;

  constructor(private config?: IConfig, logger?: ILogger) {
    this.axios = axios.create(config);
    this.config = config;
    this.logger = logger || new NoopLogger();
  }

  request<ResData>(request: IRequest): Promise<ResData> {
    this.logger.info('HTTP_REQUEST:: ', JSON.stringify(request));
    const config: AxiosRequestConfig = {
      ...request,
      timeout: 5000,
    };

    return this.axios
      .request<ResData>(config)
      .then((response: AxiosResponse<ResData>) =>
        handleResponse(response, this.logger),
      )
      .catch((error: any) => handleError(error, this.config, this.logger));
  }

  get<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'GET',
      url,
      ...request,
    });
  }

  post<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'POST',
      url,
      ...request,
    });
  }

  put<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'PUT',
      url,
      ...request,
    });
  }

  delete<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'DELETE',
      url,
      ...request,
    });
  }
}
