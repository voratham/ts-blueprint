export type Revision = {
  collectionName: string;
  companyId: string;
  projectId: string;
  versions: Array<number>;
};

export type RevisionInput = {
  collectionName: string;
  companyId: string;
  projectId: string;
};
