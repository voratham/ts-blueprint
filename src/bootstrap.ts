import { MongoConnection } from './adapters/mongo/mongo';
import { RestApiServer } from './api/rest';
import { config } from './config';

if (config.SERVER_ENABLED) {
  try {
    const mongooseOptions = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    };
    const mongoose = new MongoConnection({
      mongoURI: config.MONGO_URI,
      options: mongooseOptions,
    });
    mongoose.connect();

    const restApiServer = new RestApiServer({
      port: config.SERVER_PORT,
      hostname: config.SERVER_HOST,
    });
    restApiServer.start();
  } catch (error) {
    throw new Error(error.message);
  }
}
