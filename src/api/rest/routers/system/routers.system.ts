import * as koaRouter from 'koa-router';
export const router = new koaRouter();

router.get('/health', (ctx: koaRouter.IRouterContext) => {
  ctx.body = 'ok'
});
