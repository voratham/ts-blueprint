import * as koaRouter from 'koa-router';
import { router as masterDataRouter } from './masterData/masterData';
import { router as revisionRouter } from './revision/revision';

export const router = new koaRouter();

router.use(
  '/master-data',
  masterDataRouter.routes(),
  masterDataRouter.allowedMethods(),
);

router.use(
  '/revisions',
  revisionRouter.routes(),
  revisionRouter.allowedMethods(),
);
