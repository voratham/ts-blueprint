import { HasKeyValue, Options, Revision } from './type';

export interface IRevision {
  get(filter: HasKeyValue, options?: Options): Promise<Revision[]>;
  getBy(filter: HasKeyValue): Promise<Revision>;
  save(filter: HasKeyValue, revisionInput: Revision): Promise<Revision>;
}

export interface BaseRepository {
  findOne(query: any, options: any): Promise<any>;
  find(query?: any, options?: any): Promise<any>;
  create(data: any): Promise<any>;
  insertMany(data: any): Promise<any>;
  update(query: any, data: any): Promise<any>;
  upsert(query: any, data: any): Promise<any>;
  delete(data: any): Promise<any>;
  aggregate(aggregate: any): Promise<any>;
  aggregatePaginate(query?: any, options?: any): Promise<any>;
}
