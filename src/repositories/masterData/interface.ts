import { HasKeyValue, MasterData, Options } from './type';

export interface IMasterData {
  get(filter: HasKeyValue, options?: Options): Promise<MasterData[]>;
  save(materDataInput: Array<MasterData>): Promise<MasterData[]>;
}

export interface BaseRepository {
  findOne(query: any, options: any): Promise<any>;
  find(query?: any, options?: any): Promise<any>;
  create(data: any): Promise<any>;
  insertMany(data: any): Promise<any>;
  update(query: any, data: any): Promise<any>;
  upsert(query: any, data: any): Promise<any>;
  delete(data: any): Promise<any>;
  aggregate(aggregate: any): Promise<any>;
  aggregatePaginate(query?: any, options?: any): Promise<any>;
}
