import * as bunyan from 'bunyan'
import { config } from '../config'

const options = {
  name: config.APP_NAME,
  serializers: bunyan.stdSerializers
}

const logger = bunyan.createLogger(options)
export default logger
