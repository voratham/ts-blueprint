import { validateFailed, validatePass } from './helper';
import { ValidateDatatype } from './validateDataType';

const validateDataType = new ValidateDatatype();

describe('test validate data types [ObjectId, Number, String, Boolean, Array, Object, Enum]', () => {
  it('return validatePass when value is valid in type ObjectId', () => {
    const key = 'ObjectId';
    const value = '5cf79628d1bfc18039b60589';
    expect(validateDataType.isObjectId({ key, value })).toEqual(validatePass);
  });
  it('return validateFailed when value is not valid in type ObjectId', () => {
    const key = 'ObjectId';
    const value = '123';
    expect(validateDataType.isObjectId({ key, value })).toEqual(
      validateFailed(key, 'Field not valid type mongoose ObjectId'),
    );
  });

  it('return validatePass when value is valid in type Number', () => {
    const key = 'age';
    const value = 20;
    expect(validateDataType.isNumber({ key, value })).toEqual(validatePass);
  });
  it('return validateFailed when value is not valid in type Number', () => {
    const key = 'say';
    const value = '20';
    expect(validateDataType.isNumber({ key, value })).toEqual(
      validateFailed(key, 'Field not valid type Number'),
    );
  });

  it('return validatePass when value is valid in type String', () => {
    const key = 'say';
    const value = 'Hello world';
    expect(validateDataType.isString({ key, value })).toEqual(validatePass);
  });
  it('return validateFailed when value is not valid in type String', () => {
    const key = 'say';
    const value = 1234;
    expect(validateDataType.isString({ key, value })).toEqual(
      validateFailed(key, 'Field not valid type String'),
    );
  });

  it('return validatePass when value is valid in type Boolean', () => {
    const key = 'active';
    const value = true;
    expect(validateDataType.isBoolean({ key, value })).toEqual(validatePass);
  });
  it('return validateFailed when value is not valid in type Boolean', () => {
    const key = 'active';
    const value = 'true';
    expect(validateDataType.isBoolean({ key, value })).toEqual(
      validateFailed(key, 'Field not valid type Boolean'),
    );
  });

  it('return validatePass when value is valid in type Object', () => {
    const key = 'cars';
    const value = { type: 'Fiat', model: '500', color: 'white' };
    expect(validateDataType.isObject({ key, value })).toEqual(validatePass);
  });
  it('return validateFailed when value is not valid in type Object', () => {
    const key = 'cars';
    const value = 'Saab';
    expect(validateDataType.isObject({ key, value })).toEqual(
      validateFailed(key, 'Field not valid type Object'),
    );
  });
});
