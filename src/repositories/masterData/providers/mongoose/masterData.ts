import * as mongoose from 'mongoose';
import repositoryBuilder from 'sendit-mongoose-repository';
import * as mongooseHelper from '../../../../utils/mongoose/helper';
import { IMasterData } from '../../interface';
import { HasKeyValue, MasterData, Options } from '../../type';

const masterDataSchema = {
  companyId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
  },
  projectId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
  },
  collectionName: {
    type: String,
    index: true,
  },
  revision: {
    type: Number,
    index: true,
  },
  item: {
    type: Object,
    default: null,
  },
};

const { Repository } = repositoryBuilder('MasterData', masterDataSchema);

export class MasterDataMongooseProvider implements IMasterData {
  get(filter: HasKeyValue, options: Options): Promise<MasterData[]> {
    // use default limit because lib sendit-mongoose note limit response value
    const defaultOptionsOrOptions = Object.assign(
      { page: 1, limit: 10 },
      options,
    );
    return mongooseHelper.list<MasterData[]>({
      repo: Repository,
      filter,
      options: defaultOptionsOrOptions,
    });
  }

  save(materDataInput: Array<MasterData>): Promise<MasterData[]> {
    return mongooseHelper.create<MasterData[]>({
      repo: Repository,
      data: materDataInput,
    });
  }
}
