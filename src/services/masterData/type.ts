import { HasKeyValue } from '../type';

export type MasterDataInput = {
  companyId: string;
  projectId: string;
  collectionName: string;
  uuid: string;
  options?: {
    page?: number;
    limit?: number;
  };
};

export type MasterData = {
  companyId: string;
  projectId: string;
  collectionName: string;
  revision: number;
  item: HasKeyValue;
};

export type Revision = {
  collectionName: string;
  companyId: string;
  projectId: string;
  versions: Array<number>;
};

export type GetFrom4plImportAndSaveMasterData = {
  uuid: string;
  params: {
    page: number;
    limit: number;
  };
  newRevision: Revision;
};

export type Params = {
  page: number;
  limit: number;
};
