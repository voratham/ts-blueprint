import * as koaRouter from 'koa-router';
import * as R from 'ramda';
import { masterDataService } from '../../../../../services/masterData/masterData';
import { httpCreated, httpOk } from '../../../utils/responseHelper';
import { validateHttpInput, ValidateInput } from '../../validation/index';

export const router = new koaRouter();

router.get('/', async (ctx: koaRouter.IRouterContext) => {
  const { filter, ...options } = ctx.request.query;
  const listMasterData = await masterDataService.list(filter, options);
  httpOk(ctx, listMasterData);
});

router.post('/', async (ctx: koaRouter.IRouterContext) => {
  const masterDataInput = R.path(['request', 'body'], ctx);
  const validateObject = {
    projectId: {
      type: 'ObjectId',
    },
    companyId: {
      type: 'ObjectId',
    },
    collectionName: {
      type: 'String',
    },
    uuid: {
      type: 'String',
    },
    'options.page': {
      type: 'Number',
      optional: true,
    },
    'options.limit': {
      type: 'Number',
      optional: true,
    },
  };
  validateHttpInput({
    objectInput: masterDataInput,
    validateObject,
  } as ValidateInput);

  const createMasterData = await masterDataService.create(masterDataInput);
  httpCreated(ctx, createMasterData);
});
