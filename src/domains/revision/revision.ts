import * as R from 'ramda';

interface Revision {
  collectionName: string;
  companyId: string;
  projectId: string;
  versions: Array<number>;
}

interface IRevisionDomain {
  create({
    companyId,
    projectId,
    collectionName,
    versions,
  }: Revision): Revision;
}

const getMaxAndIncrementOne = (numbers: Array<number>): number => {
  const numbersIsEmpty = numbers.length < 1;
  if (numbersIsEmpty) {
    return 1;
  }

  const _getMaxAndIncrementOne = R.pipe(Math.max, R.add(1))(...numbers);

  return _getMaxAndIncrementOne;
};

class RevisionDomain implements IRevisionDomain {
  create({
    companyId,
    projectId,
    collectionName,
    versions = [],
  }: Revision): Revision {
    const maxRevisionVersion = getMaxAndIncrementOne(versions);
    const newRevision = {
      companyId,
      projectId,
      collectionName,
      versions: [...versions, maxRevisionVersion],
    };

    return newRevision;
  }
}

export const revisionDomain = new RevisionDomain();
