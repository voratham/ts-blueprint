import * as R from 'ramda';
import {
  DataType,
  RequireFieldProps,
  ValidateFailed,
  ValidateObject,
  ValidatePass,
} from './interface';

/**
 * Validate Pass/Failed
 */
export const validatePass: ValidatePass = {
  isValidateFailed: false,
};

export const validateFailed = (
  key: string,
  message: string,
): ValidateFailed => ({
  isValidateFailed: true,
  error: {
    [key]: message,
  },
});

/**
 * isRequireField
 */
export const requireField = ({
  key,
  value,
  optional = false,
}: RequireFieldProps): ValidatePass | ValidateFailed => {
  if (optional) {
    return validatePass;
  }

  const message = 'This field is required';
  const typeOfValue = R.type(value);
  const valueIsEmpty = R.isEmpty(value);
  if ((typeOfValue === 'Object' || typeOfValue === 'Array') && valueIsEmpty) {
    return validateFailed(key, message);
  }

  const valueIsNullOrUndefined = R.isNil(value);
  if (valueIsNullOrUndefined) {
    return validateFailed(key, message);
  }

  return validatePass;
};

/**
 * isNestedKey
 */
const splitStringToArrayByDot = (str: string): Array<string> =>
  R.split('.', str);

const getLengthOfArray = (arr: Array<string>): number => R.length(arr);

const lengthGreaterThanOne = (length: number): boolean => R.gt(length, 1);

// ('a.b') -> true | ('a') -> false
export const isNestedKey = (nestedKey: string): boolean =>
  R.pipe(
    splitStringToArrayByDot,
    getLengthOfArray,
    lengthGreaterThanOne,
  )(nestedKey);

export const isOptional = (
  key: string,
  validateObject: ValidateObject,
): boolean => R.path([key, 'optional'], validateObject);

export const getDataTypeFromObject = (
  key: string,
  validateObject: ValidateObject,
): DataType => R.path([key, 'type'], validateObject);

// nestedKey = 'a.b.c'
export const getValueFromObjectByNestedKey = (
  nestedKey: string,
  object: { [key: string]: any },
) => {
  const path = splitStringToArrayByDot(nestedKey);
  return R.path(path, object);
};
