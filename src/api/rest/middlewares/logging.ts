import { IRouterContext } from 'koa-router';

export interface ILogger {
  info(obj: Object, ...params: any[]): void;
  error(obj: Object, ...params: any[]): void;
}

export interface IOptions {
  logger: ILogger
}

const reqSerializer = (ctx: IRouterContext) => ({
  reqId: ctx.reqId,
  method: ctx.method,
  path: ctx.path,
  url: ctx.url,
  headers: ctx.headers,
  protocol: ctx.protocol,
  ip: ctx.ip,
  query: ctx.query
})

const resBodySerializer = ({ status, message }: IRouterContext) => {
  const body: any = { status, message }
  return body
}

const resSerializer = (ctx: IRouterContext) => ({
  statusCode: ctx.status,
  type: ctx.type,
  headers: (ctx.response || {}).headers,
  body: resBodySerializer(ctx.body)
})

export function loggingMiddleware({ logger }: IOptions) {
  return async function logging(ctx: IRouterContext, next: Function) {
    const startTime = Date.now()

    const req = reqSerializer(ctx)
    logger.info({ req, event: 'request' })

    try {
      await next();

      const res = resSerializer(ctx)
      const responseTime = Date.now() - startTime
      logger.info({ req, res, responseTime, event: 'response' })
    } catch (err) {
      logger.error({ req, err, event: 'error' })
    }
  }
}
