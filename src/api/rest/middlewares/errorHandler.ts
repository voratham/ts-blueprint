import { IRouterContext } from 'koa-router';
import {
  AlreadyExists,
  NotFound,
  ValidateFieldError,
} from '../../../errors/errors';

function errorMapping(error: Error) {
  const defaultBody = {
    success: false,
    data: null,
  };

  if (error instanceof NotFound) {
    return {
      status: 404,
      body: {
        ...defaultBody,
        error: {
          message: error.message,
        },
      },
    };
  }

  if (error instanceof AlreadyExists || error instanceof ValidateFieldError) {
    return {
      status: 400,
      body: {
        ...defaultBody,
        error: {
          message: error.message,
        },
      },
    };
  }

  return {
    status: 500,
    body: {
      ...defaultBody,
      error: {
        message: 'Internal server error',
      },
    },
  };
}

export async function errorHandler(ctx: IRouterContext, next: Function) {
  try {
    await next();
  } catch (error) {
    const { status, body } = errorMapping(error);
    ctx.status = status;
    ctx.body = body;
  }
}

export function errorHandlerPathNotFound(ctx: IRouterContext) {
  const { status, body } = errorMapping(
    new NotFound(`${ctx.originalUrl} not found`),
  );
  ctx.status = status;
  ctx.body = body;
}
