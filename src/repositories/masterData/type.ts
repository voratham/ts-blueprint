export type HasKeyValue = {
  [key: string]: any;
};

export type MasterData = {
  companyId: string;
  projectId: string;
  collectionName: string;
  revision: number;
  item: HasKeyValue;
};

export type Options = {
  populate?: Array<any>;
  limit: number;
  page: number;
  sort: {
    [key: string]: string | number;
  };
};
