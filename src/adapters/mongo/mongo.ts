import { connect, ConnectionOptions } from 'mongoose';

export interface IMongoConnection {
  mongoURI: string;
  options: ConnectionOptions;
}
export class MongoConnection {
  private mongoURI: string;
  private options: ConnectionOptions;
  private connected: boolean;

  constructor({ mongoURI, options }: IMongoConnection) {
    this.mongoURI = mongoURI;
    this.options = options;
  }

  connect = async () => {
    if (this.connected) {
      return;
    }
    try {
      await connect(this.mongoURI, this.options);
      this.connected = true;
    } catch (error) {
      process.exit(1);
    }
  };
}
