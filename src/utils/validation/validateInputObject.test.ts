import { validateFailed, validatePass } from './helper';
import { validateInputObject } from './validateInputObject';
describe('validateInputObject', () => {
  describe('validate object nested', () => {
    it('nested key `contact.phone` should return validatePass', () => {
      const validateObject = {
        'contact.phone': {
          type: 'String',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        contact: {
          phone: '0889967658',
        },
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('nested key `contact.phone` should return validateFailed', () => {
      const validateObject = {
        'contact.phone': {
          type: 'String',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        contact: {
          phone: 123456,
        },
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validateFailed(key, 'Field not valid type String'));
    });
  });

  describe('data type String', () => {
    it('valid value should return validatePass', () => {
      const validateObject = {
        firstName: {
          type: 'String',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        firstName: 'Foo',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('invalid value should return validateFailed', () => {
      const validateObject = {
        firstName: {
          type: 'String',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        firstName: 1234,
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validateFailed(key, 'Field not valid type String'));
    });
  });

  describe('data type Number', () => {
    it('valid value should return validatePass', () => {
      const validateObject = {
        age: {
          type: 'Number',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        age: 20,
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('invalid value should return validateFailed', () => {
      const validateObject = {
        age: {
          type: 'Number',
        },
      };
      const key = Object.keys(validateObject)[0];
      const invalidObjectInput = {
        age: '20',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput: invalidObjectInput,
        }),
      ).toEqual(validateFailed(key, 'Field not valid type Number'));
    });
  });

  describe('data type Enum', () => {
    it('valid value should return validatePass', () => {
      const validateObject = {
        gender: {
          type: 'Enum',
          enum: ['male', 'female'],
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        gender: 'male',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('invalid value should return validateFailed', () => {
      const validateObject = {
        gender: {
          type: 'Enum',
          enum: ['male', 'female'],
        },
      };
      const key = Object.keys(validateObject)[0];
      const invalidObjectInput = {
        gender: 'xxx',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput: invalidObjectInput,
        }),
      ).toEqual(
        validateFailed(
          key,
          `Value not includes in enum list [${validateObject[key].enum}]`,
        ),
      );
    });
  });

  describe('data type mongoose ObjectId', () => {
    it('valid value should return validatePass', () => {
      const validateObject = {
        companyId: {
          type: 'ObjectId',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        companyId: '5cf79628d1bfc18039b60589',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('invalid value should return validateFailed', () => {
      const validateObject = {
        companyId: {
          type: 'ObjectId',
        },
      };
      const key = Object.keys(validateObject)[0];
      const invalidObjectInput = {
        companyId: '12345',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput: invalidObjectInput,
        }),
      ).toEqual(validateFailed(key, 'Field not valid type mongoose ObjectId'));
    });
  });

  describe('data type Boolean', () => {
    it('valid value should return validatePass', () => {
      const validateObject = {
        isActive: {
          type: 'Boolean',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        isActive: true,
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('invalid value should return validateFailed', () => {
      const validateObject = {
        isActive: {
          type: 'Boolean',
        },
      };
      const key = Object.keys(validateObject)[0];
      const invalidObjectInput = {
        isActive: 'true',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput: invalidObjectInput,
        }),
      ).toEqual(validateFailed(key, 'Field not valid type Boolean'));
    });
  });

  describe('data type Array', () => {
    it('valid value should return validatePass', () => {
      const validateObject = {
        car: {
          type: 'Array',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        car: ['Saab', 'Volvo', 'BMW'],
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('invalid value should return validateFailed', () => {
      const validateObject = {
        car: {
          type: 'Array',
        },
      };
      const key = Object.keys(validateObject)[0];
      const invalidObjectInput = {
        car: 'Volvo',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput: invalidObjectInput,
        }),
      ).toEqual(validateFailed(key, 'Field not valid type Array'));
    });
  });

  describe('data type Object', () => {
    it('valid value should return validatePass', () => {
      const validateObject = {
        car: {
          type: 'Object',
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        car: { type: 'Fiat', model: '500', color: 'white' },
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('invalid value should return validateFailed', () => {
      const validateObject = {
        car: {
          type: 'Object',
        },
      };
      const key = Object.keys(validateObject)[0];
      const invalidObjectInput = {
        car: 'Volvo',
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput: invalidObjectInput,
        }),
      ).toEqual(validateFailed(key, 'Field not valid type Object'));
    });
  });

  describe('Empty value but validateObject defined optional = true', () => {
    it('normal key `fullName` should return validatePass', () => {
      const validateObject = {
        fullName: {
          optional: true,
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        firstName: 'Foo',
        lastName: 'Bar',
        fullName: null,
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('nested key `contact.address` should return validatePass', () => {
      const validateObject = {
        'contact.address': {
          optional: true,
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        contact: {
          phone: '0889967658',
          email: 'Foo@bar.com',
          address: null,
        },
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });
  });

  describe('optional = true but have value check data Type', () => {
    it('valid data type should return validate pass', () => {
      const validateObject = {
        'options.page': {
          type: 'Number',
          optional: true,
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        options: {
          page: 1,
        },
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validatePass);
    });

    it('invalid data type should return validate failed', () => {
      const validateObject = {
        'options.page': {
          type: 'Number',
          optional: true,
        },
      };
      const key = Object.keys(validateObject)[0];
      const objectInput = {
        options: {
          page: '1',
        },
      };
      expect(
        validateInputObject({
          key,
          validateObject,
          objectInput,
        }),
      ).toEqual(validateFailed(key, 'Field not valid type Number'));
    });
  });
});
