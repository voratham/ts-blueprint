export interface HasKeyValue {
  [key: string]: any;
}

export type Options = {
  populate?: Array<{
    // path value is a mongoose ref
    path: string;
  }>;
  limit: number;
  page: number;
  sort: {
    [key: string]: string | number;
  };
};
