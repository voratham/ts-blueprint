// domain
import { revisionDomain } from '../../domains/revision/revision';
// repository
import { RevisionRepository } from '../../repositories/revision/revision';
// type and interface
import { HasKeyValue, Options } from '../type';
import { IRevision } from './interface';
import { RevisionInput } from './type';

class RevisionService implements IRevision {
  list(filter: HasKeyValue, options: Options) {
    return RevisionRepository.get(filter, options);
  }

  async create(revisionInput: RevisionInput) {
    const filter = {
      projectId: revisionInput.projectId,
      collectionName: revisionInput.collectionName,
    };
    const oldRevision = await RevisionRepository.getBy(filter);
    const defaultVersionsAndRevisionInput = {
      versions: [],
      ...revisionInput,
    };

    const newRevision = revisionDomain.create(
      oldRevision || defaultVersionsAndRevisionInput,
    );
    const saveOrUpdateNewRevision = RevisionRepository.save(
      filter,
      newRevision,
    );

    return saveOrUpdateNewRevision;
  }
}

export const revisionService = new RevisionService();
