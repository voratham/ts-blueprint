import * as mongoose from 'mongoose';
import repositoryBuilder from 'sendit-mongoose-repository';
import * as mongooseHelper from '../../../../utils/mongoose/helper';
import { IRevision } from '../../interface';
import { HasKeyValue, Options, Revision } from '../../type';

export const revisionSchema = {
  companyId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
  },
  projectId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
  },
  collectionName: {
    type: String,
    index: true,
  },
  versions: {
    type: [Number],
    index: true,
  },
};

const { Repository } = repositoryBuilder('Revision', revisionSchema);

export class RevisionMongooseProvider implements IRevision {
  get(filter: HasKeyValue, options: Options) {
    return mongooseHelper.list<Revision[]>({
      repo: Repository,
      filter,
      options: options,
    });
  }

  getBy(filter: HasKeyValue) {
    return mongooseHelper.findOne<Revision>({
      repo: Repository,
      filter,
    });
  }

  save(filter: HasKeyValue, revisionInput: Revision) {
    return mongooseHelper.upsert<Revision>({
      repo: Repository,
      filter,
      data: revisionInput,
    });
  }
}
