import { Logger } from './logger';

class StreamCatcher {
  logs: any[] = [];

  write(log: any) {
    this.logs.push(log);
  }
}

describe('Logger', () => {
  let logger: Logger;
  let catcher: StreamCatcher;

  beforeAll(() => {
    catcher = new StreamCatcher();
    logger = new Logger('test', {
      stream: catcher as any,
    });
  });

  test('test event logging', () => {
    logger.info('test');
    expect(catcher.logs).toHaveLength(1);
    const log = JSON.parse(catcher.logs[0]);
    expect(log.msg).toBe('test');
  });
});
