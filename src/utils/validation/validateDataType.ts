import * as mongoose from 'mongoose';
import * as R from 'ramda';
import { validateFailed, validatePass } from './helper';
import { HasKeyValue, ValidateFailed, ValidatePass } from './interface';

interface IValidateDatatype {
  isBoolean({ key, value }: HasKeyValue): ValidatePass | ValidateFailed;
  isNumber({ key, value }: HasKeyValue): ValidatePass | ValidateFailed;
  isString({ key, value }: HasKeyValue): ValidatePass | ValidateFailed;
  isObjectId({ key, value }: HasKeyValue): ValidatePass | ValidateFailed;
  isArray({ key, value }: HasKeyValue): ValidatePass | ValidateFailed;
  isObject({ key, value }: HasKeyValue): ValidatePass | ValidateFailed;
  isEnum({ validate, key, value }: HasKeyValue): ValidatePass | ValidateFailed;
}

export class ValidateDatatype implements IValidateDatatype {
  private validate({ value, key, type }): ValidatePass | ValidateFailed {
    if (R.type(value) === type) {
      return validatePass;
    }
    return validateFailed(key, `Field not valid type ${type}`);
  }

  isBoolean({ key, value }: HasKeyValue): ValidatePass | ValidateFailed {
    return this.validate({ key, value, type: 'Boolean' });
  }

  isNumber({ key, value }: HasKeyValue): ValidatePass | ValidateFailed {
    return this.validate({ key, value, type: 'Number' });
  }

  isString({ key, value }: HasKeyValue): ValidatePass | ValidateFailed {
    return this.validate({ key, value, type: 'String' });
  }

  isObjectId({ key, value }: HasKeyValue): ValidatePass | ValidateFailed {
    const isObjectId = (id: string): boolean =>
      mongoose.Types.ObjectId.isValid(id);
    if (isObjectId(value)) {
      return validatePass;
    }
    return validateFailed(key, 'Field not valid type mongoose ObjectId');
  }

  isArray({ key, value }: HasKeyValue): ValidatePass | ValidateFailed {
    return this.validate({ key, value, type: 'Array' });
  }

  isObject({ key, value }: HasKeyValue): ValidatePass | ValidateFailed {
    return this.validate({ key, value, type: 'Object' });
  }

  isEnum({ validate, key, value }: HasKeyValue): ValidatePass | ValidateFailed {
    const enumList = R.pathOr([], [key, 'enum'], validate);
    const includesList = (value: string, list: Array<string>): boolean =>
      R.includes(value, list);

    if (includesList(value, enumList)) {
      return validatePass;
    }
    return validateFailed(key, `Value not includes in enum list [${enumList}]`);
  }
}
