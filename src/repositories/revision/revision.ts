import { IRevision } from './interface';
import { RevisionMongooseProvider } from './providers/mongoose/revision';
import { HasKeyValue, Options, Revision } from './type';

class RevisionClient {
  private provider: IRevision;

  constructor(provider: IRevision) {
    this.provider = provider;
  }

  get(filter: HasKeyValue, options?: Options) {
    return this.provider.get(filter, options);
  }

  getBy(filter: HasKeyValue) {
    return this.provider.getBy(filter);
  }

  save(filter: HasKeyValue, revisionInput: Revision) {
    return this.provider.save(filter, revisionInput);
  }
}

export const RevisionRepository = new RevisionClient(
  new RevisionMongooseProvider(),
);
