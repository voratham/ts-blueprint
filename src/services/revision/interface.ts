import { HasKeyValue, Options } from '../type';
import { Revision } from './type';
export interface IRevision {
  list(filter: HasKeyValue, options?: Options): Promise<Revision[]>;
  create(revisionInput: Revision): Promise<Revision>;
}
