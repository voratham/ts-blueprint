import {
  getDataTypeFromObject,
  getValueFromObjectByNestedKey,
  isNestedKey,
  requireField,
  validateFailed,
  validatePass,
} from './helper';

describe('requireField', () => {
  it('return validatePass when value not null or undefined', () => {
    const key = 'name';
    const value = 'romantic';
    expect(requireField({ key, value })).toEqual(validatePass);
  });
  it('return validatePass when value is null but optional=true', () => {
    const key = 'name';
    const value = null;
    const optional = true;
    expect(requireField({ key, value, optional })).toEqual(validatePass);
  });
  it('return validateFailed when value is null', () => {
    const key = 'name';
    const value = null;
    expect(requireField({ key, value })).toEqual(
      validateFailed(key, 'This field is required'),
    );
  });
  it('return validateFailed when value is undefined', () => {
    const key = 'name';
    const value = undefined;
    expect(requireField({ key, value })).toEqual(
      validateFailed(key, 'This field is required'),
    );
  });
});

describe('isNestedKey', () => {
  it('return true when key is Nested', () => {
    expect(isNestedKey('car.type')).toBeTruthy();
  });
  it('return false when key is not Nested', () => {
    expect(isNestedKey('carType')).toBeFalsy();
  });
});

describe('getDataTypeFromObject', () => {
  it('return data type String', () => {
    const key = 'age';
    const validateObject = {
      age: {
        type: 'String',
      },
    };
    expect(getDataTypeFromObject(key, validateObject)).toEqual('String');
  });
});

describe('getValueFromObjectByNestedKey', () => {
  it('return value = Fiat', () => {
    const nestedKey = 'car.type';
    const objectCar = { car: { type: 'Fiat', model: '500', color: 'white' } };
    expect(getValueFromObjectByNestedKey(nestedKey, objectCar)).toEqual('Fiat');
  });
});
