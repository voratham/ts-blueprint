import * as R from 'ramda';
import { getFrom4plImport } from '../../adapters/4pl-Import/import';
// domain
import { masterDataDomain } from '../../domains/masterData/masterData';
import { revisionDomain } from '../../domains/revision/revision';
// repository
import { MasterDataRepository } from '../../repositories/masterData/masterData';
import { RevisionRepository } from '../../repositories/revision/revision';
// type and interface
import { HasKeyValue, Options } from '../type';
import { IMasterData } from './interface';
import {
  GetFrom4plImportAndSaveMasterData,
  MasterData,
  MasterDataInput,
} from './type';

async function getFrom4plImportAndSaveMasterData({
  uuid,
  params,
  newRevision,
}: GetFrom4plImportAndSaveMasterData): Promise<MasterData[]> {
  const _getFrom4plImport = await getFrom4plImport(uuid, params);
  const _getFrom4plImportData = R.path(['data', 'data'], _getFrom4plImport);
  const { hasNext: hasNext4plImportData, page } = R.path(
    ['data'],
    _getFrom4plImport,
  );

  const newMasterData = masterDataDomain.create(
    newRevision,
    _getFrom4plImportData,
  );

  const saveNewMasterData = await MasterDataRepository.save(newMasterData);

  if (hasNext4plImportData) {
    const incrementPage = {
      page: page + 1,
      limit: params.limit,
    };
    return getFrom4plImportAndSaveMasterData({
      uuid,
      params: incrementPage,
      newRevision,
    });
  }

  return saveNewMasterData;
}

class MasterDataService implements IMasterData {
  list(filter: HasKeyValue, options?: Options) {
    return MasterDataRepository.get(filter, options);
  }

  async create(masterDataInput: MasterDataInput) {
    const filter = {
      projectId: masterDataInput.projectId,
      collectionName: masterDataInput.collectionName,
    };
    const oldRevision = await RevisionRepository.getBy(filter);
    const revisionFromMasterDataInput = {
      collectionName: masterDataInput.collectionName,
      projectId: masterDataInput.projectId,
      companyId: masterDataInput.companyId,
      versions: [],
    };

    const newRevision = revisionDomain.create(
      oldRevision || revisionFromMasterDataInput,
    );

    await RevisionRepository.save(filter, newRevision);
    const defaultParamsOrParamsFromInput = Object.assign(
      {
        page: 1,
        limit: 50,
      },
      masterDataInput.options,
    );

    const saveNewMasterData = await getFrom4plImportAndSaveMasterData({
      uuid: masterDataInput.uuid,
      params: defaultParamsOrParamsFromInput,
      newRevision,
    });

    return saveNewMasterData;
  }
}

export const masterDataService = new MasterDataService();
