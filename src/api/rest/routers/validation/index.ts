import * as R from 'ramda';
import { ValidateFieldError } from '../../../../errors/errors';
import { validateInputObject } from '../../../../utils/validation/validateInputObject';

export interface ValidateInput {
  objectInput: {
    [key: string]: any;
  };
  validateObject: {
    [key: string]: {
      type: 'String' | 'Number' | 'Boolean' | 'Array' | 'Object' | 'id' | '_id';
      optional?: boolean;
      enum?: Array<string>;
    };
  };
}

export const validateHttpInput = ({
  objectInput,
  validateObject,
}: ValidateInput): boolean | Error => {
  const validateKeys = R.keys(validateObject);
  const isError = R.reduce(
    // function
    (acc: Array<{}>, key: string) => {
      const _validateInputObject = validateInputObject({
        key,
        objectInput,
        validateObject,
      });
      const isValidateFailed =
        R.path(['isValidateFailed'], _validateInputObject) === true;

      if (isValidateFailed) {
        const errorObject = R.path(['error'], _validateInputObject);

        return {
          ...acc,
          ...errorObject,
        };
      }

      return acc;
    },
    // initial value
    {},
    // list
    validateKeys,
  );

  const haveSomeError = !R.isEmpty(isError);

  if (haveSomeError) {
    const Error = new ValidateFieldError(JSON.stringify(isError));
    console.error(Error);
    throw Error;
  }

  return true;
};
