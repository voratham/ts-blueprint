FROM node:10.15.3-alpine as builder

RUN apk add --no-cache python g++ make

WORKDIR /var/source
COPY package*.json ./
RUN npm install

COPY . .
RUN npm run build

FROM node:10.15.3-alpine

WORKDIR /var/source
COPY --from=builder /var/source/node_modules node_modules
COPY --from=builder /var/source/build build

EXPOSE 80
CMD [ "node", "build/index.js" ]