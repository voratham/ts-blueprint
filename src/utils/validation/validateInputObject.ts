import * as R from 'ramda';
import {
  getDataTypeFromObject,
  getValueFromObjectByNestedKey,
  isNestedKey,
  isOptional,
  requireField,
  validateFailed,
  validatePass,
} from './helper';
import { ValidateFailed, ValidateInputParams, ValidatePass } from './interface';
import { ValidateDatatype } from './validateDataType';

const validateDatatype = new ValidateDatatype();

export const validateInputObject = ({
  key,
  objectInput,
  validateObject,
}: ValidateInputParams): ValidatePass | ValidateFailed => {
  const value = isNestedKey(key)
    ? getValueFromObjectByNestedKey(key, objectInput)
    : R.path([key], objectInput);
  const optional = isOptional(key, validateObject);
  const dataType = getDataTypeFromObject(key, validateObject);

  // validate pass when check if(checkRequireFieldPass && dataTypeNoNeedToValidate)
  const dataTypeNoNeedToValidate =
    R.isNil(dataType) === true || R.isNil(value) === true;
  const checkRequireField = requireField({ key, value, optional });
  const checkRequireFieldPass =
    R.path(['isValidateFailed'], checkRequireField) === false;
  const checkRequireFieldFailed =
    R.path(['isValidateFailed'], checkRequireField) === true;
  if (checkRequireFieldPass && dataTypeNoNeedToValidate) {
    return validatePass;
  }

  // Error when invalid Datatype
  const invalidDatatype: boolean =
    R.type(validateDatatype[`is${dataType}`]) !== 'Function';

  if (invalidDatatype) {
    return validateFailed(
      key,
      `${key}.type = ${dataType}, It's invalid data type in validateDatatype`,
    );
  }

  // validate pass when if(checkRequireFieldPass && validateDataTypePass)
  const validateValueByDataType = validateDatatype[`is${dataType}`]({
    key,
    value,
    validate: validateObject,
  });
  const validateDataTypePass: boolean =
    R.path(['isValidateFailed'], validateValueByDataType) === false;
  if (checkRequireFieldPass && validateDataTypePass) {
    return validatePass;
  }

  // default validate
  return checkRequireFieldFailed
    ? validateFailed(key, 'This field is required')
    : validateValueByDataType;
};
