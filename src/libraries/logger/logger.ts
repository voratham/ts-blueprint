import * as bunyan from 'bunyan';
import bunyanFormat from 'bunyan-format';

interface ITracer {
  currentTrace?: {
    context: Map<any, any>;
  };
}

function addTracing(options: ILoggerOption) {
  const { tracer, tracingKey = 'x-correlation-id' } = options;

  if (tracer && tracer.currentTrace) {
    return [
      {
        [tracingKey]: tracer.currentTrace.context.get(tracingKey),
      },
    ];
  }

  return [];
}

function transformObj(obj: any) {
  // if (typeof obj === 'string') {
  //     return {
  //         event: obj
  //     }
  // }

  return obj;
}

export interface ILoggerOption {
  formatOutput?: boolean;
  stream?: NodeJS.WritableStream;
  tracer?: ITracer;
  tracingKey?: string;
}

function getStream(options: ILoggerOption) {
  if (options.stream) {
    return options.stream;
  }

  if (options.formatOutput) {
    return bunyanFormat({ outputMode: 'short' });
  }

  return null;
}

export type LogType = 'info' | 'debug' | 'warn' | 'trace' | 'error';

export interface ILogger {
  info(error: Error, ...params: any[]): void;
  info(obj: Object, ...params: any[]): void;
  info(event: string, ...params: any[]): void;
  info(obj: any, ...params: any[]): void;

  debug(error: Error, ...params: any[]): void;
  debug(obj: Object, ...params: any[]): void;
  debug(obj: any, ...params: any[]): void;

  warn(error: Error, ...params: any[]): void;
  warn(obj: Object, ...params: any[]): void;
  warn(obj: any, ...params: any[]): void;

  error(error: Error, ...params: any[]): void;
  error(obj: Object, ...params: any[]): void;
  error(obj: any, ...params: any[]): void;

  trace(error: Error, ...params: any[]): void;
  trace(obj: Object, ...params: any[]): void;
  trace(obj: any, ...params: any[]): void;
}

export class Logger {
  private logger: bunyan;
  private options: ILoggerOption;

  constructor(name: string, options: ILoggerOption = {}) {
    this.options = options;

    const logOptions: bunyan.LoggerOptions = {
      name,
    };

    const stream = getStream(options);
    if (stream) logOptions.stream = stream;

    this.logger = bunyan.createLogger(logOptions);
  }

  log(type: LogType, obj: any, params: any[] = []) {
    const logObj = transformObj(obj);
    const traceData = addTracing(this.options);

    return this.logger[type](logObj, ...params, ...traceData);
  }

  info(error: Error, ...params: any[]): void;
  info(obj: Object, ...params: any[]): void;
  info(event: string, ...params: any[]): void;

  info(obj: any, ...params: any[]) {
    this.log('info', obj, params);
  }

  debug(error: Error, ...params: any[]): void;
  debug(obj: Object, ...params: any[]): void;

  debug(obj: any, ...params: any[]) {
    this.log('debug', obj, params);
  }

  warn(error: Error, ...params: any[]): void;
  warn(obj: Object, ...params: any[]): void;

  warn(obj: any, ...params: any[]) {
    this.log('warn', obj, params);
  }

  error(error: Error, ...params: any[]): void;
  error(obj: Object, ...params: any[]): void;

  error(obj: any, ...params: any[]) {
    this.log('error', obj, params);
  }

  trace(error: Error, ...params: any[]): void;
  trace(obj: Object, ...params: any[]): void;

  trace(obj: any, ...params: any[]) {
    this.log('trace', obj, params);
  }
}

export class NoopLogger implements ILogger {
  info(error: Error, ...params: any[]): void;
  info(obj: Object, ...params: any[]): void;
  info(event: string, ...params: any[]): void;
  info(obj: any, ...params: any[]): void;
  info(_obj: any, ..._params: any[]) {}
  debug(error: Error, ...params: any[]): void;
  debug(obj: Object, ...params: any[]): void;
  debug(obj: any, ...params: any[]): void;
  debug(_obj: any, ..._params: any[]) {}
  warn(error: Error, ...params: any[]): void;
  warn(obj: Object, ...params: any[]): void;
  warn(obj: any, ...params: any[]): void;
  warn(_obj: any, ..._params: any[]) {}
  error(error: Error, ...params: any[]): void;
  error(obj: Object, ...params: any[]): void;
  error(obj: any, ...params: any[]): void;
  error(_obj: any, ..._params: any[]) {}
  trace(error: Error, ...params: any[]): void;
  trace(obj: Object, ...params: any[]): void;
  trace(obj: any, ...params: any[]): void;
  trace(_obj: any, ..._params: any[]) {}
}

export const noopLogger = new NoopLogger();
