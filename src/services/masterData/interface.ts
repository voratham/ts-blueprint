import { HasKeyValue, Options } from '../type';
import { MasterData, MasterDataInput } from './type';

export interface IMasterData {
  list(filter: HasKeyValue, options?: Options): Promise<MasterData[]>;
  create(
    filter: HasKeyValue,
    materDataInput: MasterDataInput,
  ): Promise<MasterData[]>;
}
