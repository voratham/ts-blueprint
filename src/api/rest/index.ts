import * as koaCors from '@koa/cors';
import * as gracefulShutdown from 'http-graceful-shutdown';
import * as koa from 'koa';
import * as koaBodyParser from 'koa-bodyparser';
import * as koaCompress from 'koa-compress';
import * as koaRouter from 'koa-router';
import logger from '../../logger';
import {
  errorHandler,
  errorHandlerPathNotFound,
} from './middlewares/errorHandler';
import { loggingMiddleware } from './middlewares/logging';
import { requestIdMiddleware } from './middlewares/requestId';
import { router as systemRouter } from './routers/system/routers.system';
import { router as v1Router } from './routers/v1';

export interface IRestApiServer {
  port?: number;
  hostname?: string;
  autoStart?: boolean;
}

export class RestApiServer {
  private server: koa;
  private port: number;
  private hostname: string;

  constructor({ port = 8080, hostname = '0.0.0.0' }: IRestApiServer) {
    this.port = port;
    this.hostname = hostname;
    this.server = new koa();

    this.server.use(koaCors());
    this.server.use(koaBodyParser());
    this.server.use(koaCompress());
    this.server.use(requestIdMiddleware());
    this.server.use(errorHandler);
    this.server.use(loggingMiddleware({ logger }));

    const mainRouter = new koaRouter();

    mainRouter.use(
      '/system',
      systemRouter.routes(),
      systemRouter.allowedMethods(),
    );

    mainRouter.use('/v1', v1Router.routes(), v1Router.allowedMethods());
    mainRouter.all('*', errorHandlerPathNotFound);

    this.server.use(mainRouter.routes());
  }

  start = () => {
    this.server.listen(this.port, this.hostname, () =>
      logger.info(`Server listen at http://${this.hostname}:${this.port}`),
    );
    gracefulShutdown(this.server);
  };
}
