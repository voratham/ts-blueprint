type HasKeyValue = {
  [key: string]: any;
};

interface DomainParams {
  repo: any;
  filter: HasKeyValue;
  data: HasKeyValue;
  options?: HasKeyValue;
}

interface Create {
  repo: any;
  data: HasKeyValue;
}

type StringOrNumber = string | number;

interface Options {
  populate?: Array<any>;
  limit?: number;
  page?: number;
  sort?: {
    [key: string]: StringOrNumber;
  };
}

interface Find {
  repo: any;
  filter: HasKeyValue;
  options?: Options;
}

interface FindOne {
  repo: any;
  filter: HasKeyValue;
}

export function list<T>({ repo, filter = {}, options = {} }: Find): Promise<T> {
  return repo.find(filter, options);
}

export async function findOne<T>({ repo, filter }: FindOne): Promise<T> {
  const findOne = await repo.findOne(filter);
  return findOne;
}

export async function create<T>({ repo, data }: Create): Promise<T> {
  const create = await repo.create(data);
  return create;
}

export async function upsert<T>({
  repo,
  filter,
  data,
}: DomainParams): Promise<T> {
  const alreadyExist = await repo.findOne(filter);

  if (alreadyExist) {
    const update = await repo.update(filter, data, { new: true });
    return update;
  }

  const create = await repo.create(data);
  return create;
}

export async function findOneIfAlreadyExistThrowError({
  repo,
  filter,
  options,
}: DomainParams): Promise<boolean> {
  const _options = Object.assign({}, options);
  const obj = await repo.findOne(filter, { options: _options });
  if (obj) {
    throw new Error('Already exits');
  }
  return true;
}

export async function findOneIfNotExistThrowError<T>({
  repo,
  filter,
  options,
}: DomainParams): Promise<T> {
  const _options = Object.assign({}, options);
  const obj = await repo.find(filter, { options: _options });
  if (obj.data.length < 1) {
    throw new Error(`Model:${repo.model.modelName} Not found`);
  }
  return obj;
}

export async function findOneAndUpdate<T>({
  repo,
  filter,
  data,
}: DomainParams): Promise<T> {
  const _data = Object.assign({}, data);
  const obj = await repo.update(filter, { data: _data }, { new: true });
  if (!obj) {
    throw new Error(`Model:${repo.model.modelName} Not found`);
  }
  return obj;
}

export async function findOneAndDelete<T>({
  repo,
  filter,
}: DomainParams): Promise<T> {
  const objFind = await repo.findOne(filter);
  if (!objFind) {
    throw new Error(`Model:${repo.model.modelName} Not found`);
  }
  const obj = await repo.delete(filter);
  return obj;
}
