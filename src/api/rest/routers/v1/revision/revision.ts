import * as koaRouter from 'koa-router';
import * as R from 'ramda';
import { revisionService } from '../../../../../services/revision/revision';
import { httpCreated, httpOk } from '../../../utils/responseHelper';
import { validateHttpInput, ValidateInput } from '../../validation/index';

export const router = new koaRouter();

router.get('/', async (ctx: koaRouter.IRouterContext) => {
  const { filter, ...options } = ctx.request.query;
  const revisionList = await revisionService.list(filter, options);
  httpOk(ctx, revisionList);
});

router.post('/', async (ctx: koaRouter.IRouterContext) => {
  const revisionInput = R.path(['request', 'body'], ctx);
  const validateObject = {
    projectId: {
      type: 'ObjectId',
    },
    companyId: {
      type: 'ObjectId',
    },
    collectionName: {
      type: 'String',
    },
  };
  validateHttpInput({
    objectInput: revisionInput,
    validateObject,
  } as ValidateInput);

  const createRevision = await revisionService.create(revisionInput);
  httpCreated(ctx, createRevision);
});
