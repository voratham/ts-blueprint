import axios from 'axios';
import * as R from 'ramda';
import { config } from '../../config';

const baseUri = config.SERVICE_4PL_IMPORT_URI;

type HasKeyValue = {
  [key: string]: any;
};
interface IGetFrom4plImport {
  statusCode: number;
  data: {
    data: Array<HasKeyValue>;
  };
  rows: number;
  transactionId: string;
  state: string;
  total: number;
  limit: number;
  page: number;
  hasNext: boolean;
}

export const getFrom4plImport = async (
  uuid: string,
  params: HasKeyValue = {
    page: 1,
    limit: 50,
  },
): Promise<IGetFrom4plImport> => {
  const response = await axios({
    url: `${baseUri}/transactions/${uuid}`,
    method: 'GET',
    params: params,
  });

  const result = R.path(['data'], response);

  return result;
};
