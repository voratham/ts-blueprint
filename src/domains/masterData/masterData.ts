import * as R from 'ramda';

type HasKeyValue = {
  [key: string]: any;
};

type TRevision = {
  collectionName: string;
  companyId: string;
  projectId: string;
  versions: Array<number>;
};

type THasKeyValue = {
  [key: string]: any;
};

type TMasterData = {
  collectionName: string;
  companyId: string;
  projectId: string;
  revision: number;
  item: THasKeyValue;
};

interface IMasterDataDomain {
  create(
    revisionInput: TRevision,
    items: Array<THasKeyValue>,
  ): Array<TMasterData>;
}

const getMaxValue = (numbers: Array<number>): number => {
  if (numbers.length < 1) {
    return 1;
  }

  return Math.max(...numbers);
};

const replaceKeyContainDotWithUnderscore = (item: HasKeyValue): HasKeyValue => {
  const keys = R.keys(item);
  const itemWithNewKeyName = R.reduce(
    (acc: HasKeyValue, key: string) => {
      const newKey = key.split('.').join('_');
      const valueWithNewKey = {
        [newKey]: item[key],
      };
      return {
        ...acc,
        ...valueWithNewKey,
      };
    },
    {},
    keys,
  );

  return itemWithNewKeyName;
};

class MasterDataDomain implements IMasterDataDomain {
  create(
    revisionInput: TRevision,
    items: Array<THasKeyValue>,
  ): Array<TMasterData> {
    const { companyId, projectId, collectionName } = revisionInput;
    const revision = getMaxValue(revisionInput.versions);
    const newMasterData = R.map((item: THasKeyValue): TMasterData => {
      return {
        companyId,
        projectId,
        collectionName,
        revision,
        item: replaceKeyContainDotWithUnderscore(item),
      };
    }, items);
    return newMasterData;
  }
}

export const masterDataDomain = new MasterDataDomain();
