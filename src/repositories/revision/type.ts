export type HasKeyValue = {
  [key: string]: any;
};

export type Options = {
  populate?: Array<any>;
  limit: number;
  page: number;
  sort: {
    [key: string]: string | number;
  };
};

export type Revision = {
  companyId: string;
  projectId: string;
  collectionName: string;
  versions: Array<number>;
};
