import { revisionDomain } from './revision';

describe('Revision domain', () => {
  it('method create. revisionInput.versions empty should return version = [1]', () => {
    const revisionInput = {
      companyId: '111',
      projectId: '222',
      collectionName: 'task-type',
      versions: [],
    };

    expect(revisionDomain.create(revisionInput)).toEqual({
      companyId: '111',
      projectId: '222',
      collectionName: 'task-type',
      versions: [1],
    });
  });

  it('method create. return incremental versions', () => {
    const revisionInput = {
      companyId: '111',
      projectId: '222',
      collectionName: 'task-type',
      versions: [1, 2, 3],
    };
    expect(revisionDomain.create(revisionInput)).toEqual({
      companyId: '111',
      projectId: '222',
      collectionName: 'task-type',
      versions: [1, 2, 3, 4],
    });
  });
});
