import { IMasterData } from './interface';
import { MasterDataMongooseProvider } from './providers/mongoose/masterData';
import { HasKeyValue, MasterData, Options } from './type';

class MasterDataClient {
  private provider: IMasterData;

  constructor(provider: IMasterData) {
    this.provider = provider;
  }

  get(filter: HasKeyValue, options?: Options) {
    return this.provider.get(filter, options);
  }

  save(masterDataInput: Array<MasterData>) {
    return this.provider.save(masterDataInput);
  }
}

export const MasterDataRepository = new MasterDataClient(
  new MasterDataMongooseProvider(),
);
