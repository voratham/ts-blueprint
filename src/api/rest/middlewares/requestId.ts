import uuid = require('uuid')

declare module 'koa-router' {
  interface IRouterContext {
    reqId?: string;
  }
}

export interface IOptions {
  header?: string

}

export const requestIdMiddleware = (options: IOptions = {}) => {
  const { header = 'X-Request-Id' } = options

  return (ctx, next) => {
    const reqId = ctx.request.get(header) || uuid.v4()
    ctx.reqId = reqId
    ctx.set(header, reqId)
    return next()
  }
}
